#!/bin/bash
#############################################################
#          9mavrC2016wrap - - Tree Command Script           #
#               Created by: Christo Mavrick                 #
#              Date created: April 20th, 2016               #
#    This script takes in up to 3 parameters, 2 switches    #
#  and 1 file path (all optional) and outputs a tree based  #
#   on the switches and the sent path, if no path is sent   #
#  the PWD is the path used. If the '-d' switch is selected #
#  then only directories will be shown. If the '-s' switch  #
#   is selected then file sizes will display as well. Any   #
#       combination of switches/options can be used.        #
#############################################################

##############################################
#        SOURCING AND INITIALIZATION         #
##############################################
source 9mavrC2016funcs.sh
d_switch=false
s_switch=false
setcolor

##############################################
#        GET OPTIONS AND SET SWITCHES        #
##############################################
while getopts ":ds" options; do
    case $options in
	d  )
	    d_switch=true
	    ;;
	s  )
	    s_switch=true
	    ;;
	\?  ) echo 'usage: 9mavrC2016wrap [-d] [-s] [PathToTarget]' #anything not d or s was set as an option
	      exit 1;;
    esac
done

##############################################
#               PRE-PROCESSING               #
##############################################
shift $(( $OPTIND - 1 )) #shift out switch options so only path is left
startingDir=$1 #start startingDir off as the path the user sent
initvars $s_switch $d_switch

##############################################
#               ERROR CHECKING               #
##############################################
if [[ $# -eq 1 ]]; then #1 parameter was sent after the switches
    if [[ ! -d $startingDir ]]; then
	printf "${RED}Error: Directory does not exist.\n${NONE}"
	exit 2
    fi
    path=$path"/"$startingDir #add startingDir to our path that beforehand was just the PWD
    printf "${YELLOW}$startingDir\n${NONE}"
elif [[ $# -eq 0 ]]; then #no parameter was sent after the switches
    printf "${YELLOW}.\n${NONE}"
else #more than 1 parameter was sent after the switches
    printf "${RED}Error: Too many parameters.\n${NONE}"
    exit 3
fi

##############################################
#               MAIN ALGORITHM               #
#           (see 9mavrC2016funcs)            #
##############################################
mytree $path #first call to mytree with only the path

###############################################
#               POST-PROCESSING               #
###############################################
if [[ $d_switch == false ]]; then
    printf "${YELLOW}\n$dirCount directories, $fileCount files\n${NONE}"
else #d_switch is true
    printf "${YELLOW}\n$dirCount directories\n${NONE}"
fi
