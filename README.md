# BASH Tree Command

A re-creation of the BASH "tree" command using recursive functions as the primary logic.

-----------------------------------------------------------------------------------------

The "wraps" script takes in up to 3 parameters, 2 switches and 1 file path (all optional) and outputs a tree based on the switches and the sent path, if no path is sent the PWD is the path used.

If the '-d' switch is selected then only directories will be shown. If the '-s' switch is selected then file sizes will display as well.

Any combination of switches/options can be used. 

-----------------------------------------------------------------------------------------

P.S: The "wraps" script is the only one that should ever be called, the "funcs" script is solely there to be sourced by it.