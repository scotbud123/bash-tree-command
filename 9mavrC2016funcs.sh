#!/bin/bash
#############################################################
# 9mavrC2016funcs-Functions Library for Tree Command Script #
#               Created by: Christo Mavrick                 #
#              Date created: April 20th, 2016               #
#   This function library has 3 functions to be called on   #
#      and used by, mainly the script "9mavrC2016wrap".     #
#############################################################

################################################
#   Function to initialize variables for use   #
#  in Tree Command Script. Takes in 2 true or  #
#  false values and sets the s and d options   #
#  			based on these values.			   #
################################################
#------------------------------------------------------#
function initvars {
    npfx="|-- "
    lpfx="\`-- "
    nIncr="|   "
    lIncr="    "

    s_option=$1
    d_option=$2

    size=""

    path=$(pwd)

    dirCount=0
    fileCount=0
}

################################################
#   Function to initialize specifically color  #
#   variables for use in Tree Command Script   #
#     and specify which should be used for     #
#  		directories and which for files.	   #
################################################
#------------------------------------------------------#
function setcolor {
    NONE='\033[0m'
    RED='\033[31m'
    GREEN='\033[32m'
    YELLOW='\033[33m'
    PURPLE_PINK='\033[1;35m'
    LIGHTGREEN='\033[1;32m'

    DIRCOLOR=${GREEN}
    FILECOLOR=${PURPLE_PINK}
}

##################################################
#   This function is the core of the algorithm.  #
#    It recursively calls itself anytime a new   #
#     directory is encountered with different    #
#    increments passed based on the situation.   #
#												 #
#    Variables with a local scope are decalred	 #
#    to determine how many files or directories  #
#      remain, if it's the last one different    #
#	   actions needed to be taken accordingly.	 #
#												 #
#	 If the '-d' switch is on only directories	 #
#	will be displayed. If the '-s' switch is on  #
#	the file size will be displayed with every	 #
#	 line. See tree man page for more details	 #
#	 on the '-d' and '-s' switches as they are	 #
#				nearly identical.				 #
##################################################
#------------------------------------------------------#
function mytree {
    pushd $1 1>&- #this closes the stdout for this commmand
    declare -i numFiles=$(ls -A|grep -c ^) #declare as an int, automatically makes scope local
    declare -i numDirs=$(ls -l|grep -c ^d) #declare as an int, automatically makes scope local

    for i in *
    do
      if [[ $s_option == true ]]; then
	  if [[ $numFiles -ge 1 && $numDirs -ge 1 ]]; then
	      size=$(stat -c %s "$i")
	      size=" { "$size" }"
	  fi
      fi
      if [[ $d_option == false ]]; then
	  if [[ $numFiles -ne 1 ]]; then
	      if [[ -d $i ]]; then
		  dirCount=$(( $dirCount + 1 ))
		  printf "${DIRCOLOR}$2$npfx$i$size\n${NONE}"
		  mytree $i "$2$nIncr"
	      elif [[ $numFiles -ge 1 ]]; then
		  fileCount=$(( $fileCount + 1 ))
		  printf "${FILECOLOR}$2$npfx$i$size\n${NONE}"
	      fi
	  else #numFiles is 1
	      if [[ -d $i ]]; then
		  dirCount=$(( $dirCount + 1 ))
		  printf "${DIRCOLOR}$2$lpfx$i$size\n${NONE}"
		  mytree $i "$2$lIncr"
	      elif [[ $numFiles -ge 1 ]]; then
		  fileCount=$(( $fileCount + 1 ))
		  printf "${FILECOLOR}$2$lpfx$i$size\n${NONE}"
	      fi
	  fi
	  (( numFiles-- )) #C-Style -- operrator since I declared it as an int
      else #d option is true
	  if [[ $numDirs -ne 1 ]]; then
	      if [[ -d $i ]]; then
		  dirCount=$(( $dirCount + 1 ))
                  printf "${DIRCOLOR}$2$npfx$i$size\n${NONE}" 
		  (( numDirs-- )) #C-Style -- operrator since I declared it as an int
                  mytree $i "$2$nIncr"
	      fi
	  else #numDirs is less than or equal to 1
	      if [[ -d $i ]]; then
                  dirCount=$(( $dirCount + 1 ))
		  printf "${DIRCOLOR}$2$lpfx$i$size\n${NONE}"
		  (( numDirs-- )) #C-Style -- operrator since I declared it as an int
                  mytree $i "$2$lIncr"
	      fi
	  fi
      fi
    done

    popd 1>&- #this closes the stdout for this commmand
}
